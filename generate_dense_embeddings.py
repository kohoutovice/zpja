#!/usr/bin/env python3
# Copyright (c) Facebook, Inc. and its affiliates.
# All rights reserved.
#
# This source code is licensed under the license found in the
# LICENSE file in the root directory of this source tree.

"""
 Command line tool that produces embeddings for a large documents base based on the pretrained ctx & question encoders
 Supposed to be used in a 'sharded' way to speed up the process.
"""
import logging
import math
import os
import pathlib
import pickle
from typing import List, Tuple

import hydra
import numpy as np
import torch
from omegaconf import DictConfig, OmegaConf
from torch import nn

from dpr.data.biencoder_data import BiEncoderPassage
from dpr.options import set_cfg_params_from_state, setup_cfg_gpu, setup_logger

from dpr.utils.data_utils import Tensorizer
from dpr.utils.model_utils import (
    setup_for_distributed_mode,
    get_model_obj,
    load_states_from_checkpoint,
    move_to_device,
)

from models import *
import argparse
from utils import *

logger = logging.getLogger()
setup_logger(logger)

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

models = {
    'DPRReduced': DPRReduced,
    'DPRReducedLayerNorm': DPRReducedLayerNorm,
    'DPRReducedLayerNormTransformerWeights': DPRReducedLayerNormTransformerWeights,
    'DPROriginal': DPROriginal,
    'DPRReducedLayerNormTransformerWeights512':DPRReducedLayerNormTransformerWeights512,
    'DPRScratch': DPRScratch
}



def gen_ctx_vectors(
    cfg: DictConfig,
    ctx_rows: List[Tuple[object, BiEncoderPassage]],
    model: nn.Module,
    tensorizer: Tensorizer,
    insert_title: bool = True,
) -> List[Tuple[object, np.array]]:
    n = len(ctx_rows)
    bsz = cfg.batch_size
    total = 0
    results = []
    for j, batch_start in enumerate(range(0, n, bsz)):
        batch = ctx_rows[batch_start : batch_start + bsz]
        batch_token_tensors = tensorizer(
                                text=[ctx[1].title for ctx in batch],
                                text_pair=[ctx[1].text for ctx in batch],
                                truncation=True, padding=True, return_tensors='pt'
                            ).input_ids

        batch_token_tensors_attn = tensorizer(
                                text=[ctx[1].title for ctx in batch],
                                text_pair=[ctx[1].text for ctx in batch],
                                truncation=True, padding=True, return_tensors='pt'
                            ).attention_mask

        ctx_ids_batch = move_to_device(batch_token_tensors, cfg.device)
        ctx_seg_batch = move_to_device(torch.zeros_like(ctx_ids_batch), cfg.device)
        ctx_attn_mask = move_to_device(batch_token_tensors_attn, cfg.device)
        with torch.no_grad():
            out = model.get_context_vectors(ctx_ids_batch.to(device), ctx_attn_mask.to(device))
        out = out.cpu()

        ctx_ids = [r[0] for r in batch]
        extra_info = []
        if len(batch[0]) > 3:
            extra_info = [r[3:] for r in batch]

        assert len(ctx_ids) == out.size(0)
        total += len(ctx_ids)

        # TODO: refactor to avoid 'if'
        if extra_info:
            results.extend([(ctx_ids[i], out[i].view(-1).numpy(), *extra_info[i]) for i in range(out.size(0))])
        else:
            results.extend([(ctx_ids[i], out[i].view(-1).numpy()) for i in range(out.size(0))])

        if total % 100 == 0:
            logger.info("Encoded passages %d, len(results) = %d", total, len(results))

        if len(results) > 1000000:
            file = cfg.out_file + "_" + str(cfg.shard_id) + "_" + str(total)
            pathlib.Path(os.path.dirname(file)).mkdir(parents=True, exist_ok=True)
            logger.info("Writing results to %s" % file)
            with open(file, mode="wb") as f:
                pickle.dump(results, f)
                results = []

    if results:
        file = cfg.out_file + "_" + str(cfg.shard_id) + "_" + str(total)
        pathlib.Path(os.path.dirname(file)).mkdir(parents=True, exist_ok=True)
        logger.info("Writing results to %s" % file)
        with open(file, mode="wb") as f:
            pickle.dump(results, f)
            results = []

    return results


@hydra.main(config_path="conf", config_name="gen_embs")
def main(cfg: DictConfig):
    assert cfg.ctx_src, "Please specify passages source as ctx_src param"

    logger.info("CFG:")
    logger.info("%s", OmegaConf.to_yaml(cfg))

    model = models[cfg.model]()
    model.to(device)
    model.eval()

    if cfg.model_file:
        load_checkpoint(model, torch.optim.AdamW(model.parameters(), lr=5e-5), cfg.model_file)

    tensorizer = model.ctx_tokenizer

    logger.info("reading data source: %s", cfg.ctx_src)
    ctx_src = hydra.utils.instantiate(cfg.ctx_sources[cfg.ctx_src])
    all_passages_dict = {}
    ctx_src.load_data_to(all_passages_dict)
    all_passages = [(k, v) for k, v in all_passages_dict.items()]

    shard_size = math.ceil(len(all_passages) / cfg.num_shards)
    start_idx = cfg.start_idx
    end_idx = start_idx + shard_size

    logger.info(
        "Producing encodings for passages range: %d to %d (out of total %d)",
        start_idx,
        end_idx,
        len(all_passages),
    )
    shard_passages = all_passages[start_idx:end_idx]

    gen_ctx_vectors(cfg, shard_passages, model, tensorizer, True)

    logger.info("Total passages processed.")

main()
