# Author: Jan Jurca xjurca08@studs.fit.vutbr.cz


from utils import *
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-i',"--input", action="store",type=str,default="results.json", help="input results json")
parser.add_argument('-o',"--output", action="store",type=str,default="results.png", help="output image filename")
parser.add_argument('-s',"--show", action="store_true", help="should be image showed")
args = parser.parse_args()


with open(args.input, 'r') as f:
    data = json.load(f)
    plot(data, args.output, args.show)
