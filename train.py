# Author: Jan Jurca xjurca08@studs.fit.vutbr.cz


from transformers import AutoTokenizer
import transformers
from utils import *
from models import *
from torch.utils.data import DataLoader
import torch
import torch.nn as nn
import argparse
import os
import pickle
import json
import time


models = {
    'DPRReduced': DPRReduced,
    'DPRReducedLayerNorm': DPRReducedLayerNorm,
    'DPRReducedLayerNormTransformerWeights': DPRReducedLayerNormTransformerWeights,
    'DPROriginal': DPROriginal,
    'DPRReducedLayerNormTransformerWeights512':DPRReducedLayerNormTransformerWeights512,
    'DPRScratch': DPRScratch,
}

identificator = str(int(time.time()))

parser = argparse.ArgumentParser()
parser.add_argument('-b',"--batchsize", action="store",type=int,default=16, help="batchsize")
parser.add_argument('-e',"--epoch", action="store",type=int,default=5, help="num of epochs")
parser.add_argument("--dev", action="store_true",default=False, help="Dev mode loads just a fraction of dataset vectors")
parser.add_argument("--validation", action="store_true",default=False, help="Only validate on selected checkpoint")
parser.add_argument("--checkpoint", action="store",default=None, help="Model checkpint file")
parser.add_argument("--use-all", action="store_true",default=False, help="wheter to use all training data (not split to dev)")
parser.add_argument("--test-as-dev", action="store_true",default=False, help="wheter to use all training data (not split to dev)")
parser.add_argument("--model", action="store",default='DPR', help="Possible values: " + ", ".join(models.keys()))
parser.add_argument("--freeze", action="store",default=0.5, type=float, help="How much to freeze")

args = parser.parse_args()

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

model = models[args.model](freeze_params=args.freeze)

train_questions, train_contexts, dev_questions, dev_contexts, val_questions, val_contexts = get_data(dev=args.dev,all=args.use_all, tdev=args.test_as_dev)

train_query_encodings = model.question_tokenizer(train_questions, truncation=True, padding=True, return_tensors='pt')
train_context_encodings = model.ctx_tokenizer(train_contexts, truncation=True, padding=True, return_tensors='pt')

dev_query_encodings = model.question_tokenizer(dev_questions, truncation=True, padding=True, return_tensors='pt')
dev_context_encodings = model.ctx_tokenizer(dev_contexts, truncation=True, padding=True, return_tensors='pt')

val_query_encodings = model.question_tokenizer(val_questions, truncation=True, padding=True, return_tensors='pt')
val_context_encodings = model.ctx_tokenizer(val_contexts, truncation=True, padding=True, return_tensors='pt')

set_val_context_encodings = model.ctx_tokenizer(list(set(val_contexts).union(set(train_contexts)).union(set(dev_contexts))), truncation=True, padding=True, return_tensors='pt')


model.train()
model.to(device)

criterion = nn.NLLLoss()
optimizer = torch.optim.AdamW(model.parameters(), lr=5e-5)


if args.checkpoint:
    load_checkpoint(model, optimizer, args.checkpoint)



train_loader = torch.utils.data.DataLoader(DatasetCustom(train_context_encodings, train_query_encodings), batch_size=args.batchsize, shuffle=True)
dev_loader = torch.utils.data.DataLoader(DatasetCustom(dev_context_encodings, dev_query_encodings), batch_size=args.batchsize, shuffle=True)
val_loader = torch.utils.data.DataLoader(DatasetCustom(val_context_encodings, val_query_encodings), batch_size=args.batchsize, shuffle=False)
set_val_loader = torch.utils.data.DataLoader(DatasetCTX(set_val_context_encodings), batch_size=args.batchsize, shuffle=False)


def eval():
    model.eval()
    queries_vectors_list = []
    contexts_vectors_list = []
    contexts_strings_list = []
    set_contexts_vector_list = []
    set_contexts_strings_list = []
    set_contexts_ids = dict()
    queries_contexts_solution_ids = []

    for batch_idx, (question_input_ids, question_attention_mask, context_input_ids, context_attention_mask) in enumerate(val_loader):
        queries_vectors_list.extend(model.get_query_vector(question_input_ids, question_attention_mask).tolist())
        contexts_vectors_list.extend((context_input_ids, context_attention_mask))
        for ctx in context_input_ids.tolist():
            contexts_strings_list.append(model.ctx_tokenizer.decode(ctx))
        if batch_idx % 100 == 0:
            print(f"Eval batch [{batch_idx}/{len(val_loader)}]")


    for i, (context_input_ids, context_attention_mask) in enumerate(set_val_loader):
        set_contexts_vector_list.extend(model.get_context_vectors(context_input_ids, context_attention_mask).tolist())
        for ctx in context_input_ids.tolist():
            set_contexts_strings_list.append(model.ctx_tokenizer.decode(ctx))
        if i % 100 == 0:
            print(f"Eval batch ctx [{i}/{len(set_val_loader)}]")
    queries_vectors_list = torch.tensor(queries_vectors_list)
    set_contexts_vector_list = torch.tensor(set_contexts_vector_list)

    for i, string in enumerate(set_contexts_strings_list):
        set_contexts_ids[string] = i

    for i, ctx in enumerate(contexts_strings_list):
        queries_contexts_solution_ids.append(set_contexts_ids[ctx])

    similarity_matrix = torch.matmul(queries_vectors_list, torch.transpose(set_contexts_vector_list, 0, 1))

    total_count = len(contexts_strings_list)
    print("total_count", total_count)
    results = []
    for k in range(1,101):
        topk = torch.topk(similarity_matrix, k)
        correct_count = 0
        for i, indicies in enumerate(topk.indices.tolist()):
            if queries_contexts_solution_ids[i] in indicies:
                correct_count += 1
        result = 100*(correct_count/total_count)
        results.append((k, result))
        #print(f"Top-{k}: ", result)
    print(results)

    plot_data = [{'name':model.__class__.__name__, 'topk': results}]
    loaded_results = []
    try:
        with open("results.json",'r') as f:
            loaded_results = json.load(f)
    except FileNotFoundError:
        pass
    loaded_results.extend(plot_data)
    with open("results.json",'w') as f:
        json.dump(loaded_results, f)

    plot(plot_data)
    model.train()
    return


def test(batchnum = None):
    test_batch_loss = []
    model.eval()
    for batch_idx, (question_input_ids, question_attention_mask, context_input_ids, context_attention_mask) in enumerate(dev_loader):
        logits, labels = model(context_input_ids, context_attention_mask, question_input_ids, question_attention_mask)
        loss = criterion(logits, labels.to(device))
        test_batch_loss.append(loss.item())
        if batchnum:
            if batch_idx>batchnum:
                break
    model.train()
    return sum(test_batch_loss)/len(test_batch_loss)

def train():
    for epoch in range(args.epoch):
        train_epoch_loss = []
        train_batch_loss = []

        for batch_idx, (question_input_ids, question_attention_mask, context_input_ids, context_attention_mask) in enumerate(train_loader):
            optimizer.zero_grad()
            logits, labels = model(context_input_ids, context_attention_mask, question_input_ids, question_attention_mask)
            loss = criterion(logits, labels.to(device))
            loss.backward()
            optimizer.step()
            train_batch_loss.append(loss.item())
            train_epoch_loss.append(loss.item())

            if batch_idx % 1000 == 0:
                test_loss = test(50)
                print(f"Epoch: [{epoch}/{args.epoch}] Batch : [{int(batch_idx)}/{len(train_loader)}]  Loss : {sum(train_batch_loss)/len(train_batch_loss)} Testing Loss: {test_loss}")
                train_batch_loss = []

        test_loss = test()
        print(f"Whole Epoch: [{epoch}/{args.epoch}] Loss : {sum(train_epoch_loss)/len(train_epoch_loss)} Testing Loss: {test_loss}")
        eval()
        print("saved_models/" + model.__class__.__name__ + "_" +  identificator + "_" + str(epoch) + ".torch")
        state = {'epoch': epoch + 1, 'state_dict': model.state_dict(), 'optimizer': optimizer.state_dict() }
        torch.save(state, "saved_models/" + model.__class__.__name__ + "_" +  identificator + "_" + str(epoch) + ".torch")


if not args.validation:
    train()

eval()
