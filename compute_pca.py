# Author: Jan Jurca xjurca08@studs.fit.vutbr.cz

import argparse
import pickle
import glob
import numpy as np
import pathlib
from sklearn.decomposition import PCA

parser = argparse.ArgumentParser()
parser.add_argument("--glob", action="store", help="Glob expresion for imput file")
parser.add_argument("--dimension", action="store", type=int, help="Glob expresion for imput file")
parser.add_argument("--save", action="store", type=str, help="save folder name")
parser.add_argument("--limit", action="store", type=int, help="Limit the index of data for svd")



args = parser.parse_args()

all_data = []
for filename in glob.glob(args.glob):
    with open(filename, 'rb') as f:
        print("Loading:", filename)
        data = pickle.load(f)
        all_data.extend([x[1].tolist() for x in data])
        if len(all_data) > args.limit:
            break
print("Transforming to numpy")
all_data = np.array(all_data)
all_data = all_data[:args.limit]
print("Computing pca")

pca = PCA(n_components=args.dimension)
pca.fit(all_data)

print("all_data", all_data.shape)

pathlib.Path(args.save).mkdir(parents=True, exist_ok=True)

with open(args.save.rstrip("/") + "/pca.p", 'wb') as target:
    pickle.dump(pca, target)

for filename in glob.glob(args.glob):
    orig_data = []
    orig_metadata = []
    reduced_data = None
    with open(filename, 'rb') as f:
        print("Transforming file:", filename)
        data = pickle.load(f)
        orig_data.extend([x[1].tolist() for x in data])
        orig_metadata.extend([x[0] for x in data])
        reduced_data = pca.transform(np.array(orig_data))

        print("reduced:", reduced_data.shape)

    with open(args.save.rstrip("/") + "/" + filename.split("/")[-1] + "_transformed_to_" + str(args.dimension), 'wb') as target:
        res = [(x,np.array(y, dtype=np.float32)) for x, y in zip(orig_metadata, reduced_data.tolist())]
        pickle.dump(res, target)
