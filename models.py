# Author: Jan Jurca xjurca08@studs.fit.vutbr.cz


from transformers import AutoTokenizer
from transformers import DPRContextEncoder, DPRContextEncoderTokenizer, DPRContextEncoderTokenizerFast
from transformers import DPRQuestionEncoder, DPRQuestionEncoderTokenizer, DPRQuestionEncoderTokenizerFast
import transformers
from utils import *
from torch.utils.data import DataLoader
import torch
import torch.nn as nn
from transformers import ElectraModel, ElectraConfig, ElectraTokenizerFast, PreTrainedModel
import pickle
from sklearn.decomposition import PCA
import os
from transformers import BertTokenizer, BertModel, BertTokenizerFast

class DotProductModel(nn.Module):
    def __init__(self):
        super(DotProductModel, self).__init__()

    # Freeze the first self.freeze_params % layers
    def freeze_layers(self):
        num_query_layers = sum(1 for _ in self.query_model.parameters())
        num_passage_layers = sum(1 for _ in self.passage_model.parameters())

        for parameters in list(self.query_model.parameters())[:int(self.freeze_params * num_query_layers)]:
            parameters.requires_grad = False

        for parameters in list(self.query_model.parameters())[int(self.freeze_params * num_query_layers):]:
            parameters.requires_grad = True

        for parameters in list(self.passage_model.parameters())[:int(self.freeze_params * num_passage_layers)]:
            parameters.requires_grad = False

        for parameters in list(self.passage_model.parameters())[int(self.freeze_params * num_passage_layers):]:
            parameters.requires_grad = True

    def get_context_vectors(self, context_input_ids, context_attention_mask):
        vec = self.passage_model(input_ids=context_input_ids, attention_mask=context_attention_mask)
        vec = self.query_to_dense(vec.pooler_output)
        return vec

    def get_query_vector(self, query_input_ids, query_attention_mask):
        vec = self.query_model(input_ids=query_input_ids, attention_mask=query_attention_mask)
        vec = self.query_to_dense(vec.pooler_output)
        return vec

    def forward(self, context_input_ids, context_attention_mask, query_input_ids, query_attention_mask):
        dense_query = self.query_model(input_ids=query_input_ids, attention_mask=query_attention_mask)
        dense_passage = self.passage_model(input_ids=context_input_ids.squeeze(), attention_mask=context_attention_mask.squeeze())

        dense_query = dense_query['pooler_output']
        dense_passage = dense_passage['pooler_output']

        dense_query = self.query_to_dense(dense_query)
        dense_passage = self.passage_to_dense(dense_passage)

        similarity_matrix = torch.matmul(dense_query, torch.transpose(dense_passage, 0, 1))

        batchsize = similarity_matrix.size()[0]
        labels = torch.tensor(list(range(batchsize)))

        logits = self.log_softmax(similarity_matrix)
        return logits, labels.flatten()



class DPRReduced(DotProductModel):
    def __init__(self, dense_size=256, freeze_params=0.5):
        '''
        :dense_size : the dimension to which the DPR has to encode
        :freeze_params : the percentage of the parameters to be frozen
        '''
        super(DPRReduced, self).__init__()
        self.dense_size = dense_size
        self.ctx_tokenizer = DPRContextEncoderTokenizerFast.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.passage_model = DPRContextEncoder.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.question_tokenizer = DPRQuestionEncoderTokenizerFast.from_pretrained('facebook/dpr-question_encoder-single-nq-base')
        self.query_model = DPRQuestionEncoder.from_pretrained('facebook/dpr-question_encoder-single-nq-base')

        self.freeze_params = freeze_params
        self.passage_to_dense = nn.Sequential(nn.Linear(768, dense_size * 2),
                                              nn.ReLU(),
                                              nn.Linear(dense_size * 2, dense_size),
                                              nn.GELU())

        self.query_to_dense = nn.Sequential(nn.Linear(768, dense_size * 2),
                                            nn.ReLU(),
                                            nn.Linear(dense_size * 2, dense_size),
                                            nn.GELU())
        self.log_softmax = nn.LogSoftmax(dim=1)
        self.freeze_layers()


class DPRReducedLayerNorm(DotProductModel):
    def __init__(self, dense_size=256, freeze_params=0.0):
        '''
        :dense_size : the dimension to which the DPR has to encode
        :freeze_params : the percentage of the parameters to be frozen
        '''
        super(DPRReducedLayerNorm, self).__init__()
        self.dense_size = dense_size
        self.ctx_tokenizer = DPRContextEncoderTokenizerFast.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.passage_model = DPRContextEncoder.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.question_tokenizer = DPRQuestionEncoderTokenizerFast.from_pretrained('facebook/dpr-question_encoder-single-nq-base')
        self.query_model = DPRQuestionEncoder.from_pretrained('facebook/dpr-question_encoder-single-nq-base')

        self.freeze_params = freeze_params
        self.passage_to_dense = nn.Sequential(nn.Linear(768, dense_size * 2),
                                              nn.ReLU(),
                                              nn.Linear(dense_size * 2, dense_size),
                                              nn.LayerNorm(dense_size),
                                              nn.GELU())

        self.query_to_dense = nn.Sequential(nn.Linear(768, dense_size * 2),
                                            nn.ReLU(),
                                            nn.Linear(dense_size * 2, dense_size),
                                            nn.LayerNorm(dense_size),
                                            nn.GELU())
        self.log_softmax = nn.LogSoftmax(dim=1)
        self.freeze_layers()


class DPRReducedLayerNormTransformerWeights(DotProductModel):
    def __init__(self, dense_size=256, freeze_params=0.5):
        '''
        :dense_size : the dimension to which the DPR has to encode
        :freeze_params : the percentage of the parameters to be frozen
        '''
        super(DPRReducedLayerNormTransformerWeights, self).__init__()
        self.dense_size = dense_size
        self.ctx_tokenizer = DPRContextEncoderTokenizerFast.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.passage_model = DPRContextEncoder.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.question_tokenizer = DPRQuestionEncoderTokenizerFast.from_pretrained('facebook/dpr-question_encoder-single-nq-base')
        self.query_model = DPRQuestionEncoder.from_pretrained('facebook/dpr-question_encoder-single-nq-base')

        self.freeze_params = freeze_params
        self.passage_to_dense = nn.Sequential(nn.Linear(768, dense_size * 2),
                                              nn.ReLU(),
                                              nn.Linear(dense_size * 2, dense_size),
                                              nn.LayerNorm(dense_size),
                                              nn.GELU())

        self.query_to_dense = nn.Sequential(nn.Linear(768, dense_size * 2),
                                            nn.ReLU(),
                                            nn.Linear(dense_size * 2, dense_size),
                                            nn.LayerNorm(dense_size),
                                            nn.GELU())
        self.log_softmax = nn.LogSoftmax(dim=1)
        self.freeze_layers()
        self.init_weights(DPRQuestionEncoder)


    def init_weights(self, clz):
        for ch in self.children():
            if issubclass(ch.__class__, torch.nn.Module) and not issubclass(ch.__class__, PreTrainedModel):
                ch.apply(lambda module: clz._init_weights(self.passage_model, module))



class DPROriginal(DotProductModel):
    def __init__(self, dense_size=768, freeze_params=0.0):
        '''
        :dense_size : the dimension to which the DPR has to encode
        :freeze_params : the percentage of the parameters to be frozen
        '''
        super(DPROriginal, self).__init__()
        self.dense_size = dense_size
        self.ctx_tokenizer = DPRContextEncoderTokenizerFast.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.passage_model = DPRContextEncoder.from_pretrained('facebook/dpr-ctx_encoder-single-nq-base')
        self.question_tokenizer = DPRQuestionEncoderTokenizerFast.from_pretrained('facebook/dpr-question_encoder-single-nq-base')
        self.query_model = DPRQuestionEncoder.from_pretrained('facebook/dpr-question_encoder-single-nq-base')

        self.freeze_params = freeze_params
        self.freeze_layers()


    def get_context_vectors(self, context_input_ids, context_attention_mask):
        vec = self.passage_model(input_ids=context_input_ids, attention_mask=context_attention_mask)
        return vec.pooler_output

    def get_query_vector(self, query_input_ids, query_attention_mask):
        vec = self.query_model(input_ids=query_input_ids, attention_mask=query_attention_mask)
        return vec.pooler_output

    def forward(self, context_input_ids, context_attention_mask, query_input_ids, query_attention_mask):
        raise Exception("Not for training")

class DPRReducedLayerNormTransformerWeights512(DPRReducedLayerNormTransformerWeights):
    """docstring for DPRReducedLayerNormTransformerWeights512."""

    def __init__(self, dense_size=512, freeze_params=0.5):
        super(DPRReducedLayerNormTransformerWeights512, self).__init__(dense_size, freeze_params)


class DPRReducedPCA256(DPROriginal):
    """docstring for DPRReducedLayerNormTransformerWeights512."""

    def __init__(self, dense_size=768, freeze_params=0.0):
        super(DPRReducedPCA256, self).__init__(dense_size, freeze_params)
        with open(os.path.dirname(__file__) + "/pca_256.p", 'rb') as target:
            self. pca = pickle.load(target)
        self.dense_size = 256

    def get_context_vectors(self, context_input_ids, context_attention_mask):
        vec = self.passage_model(input_ids=context_input_ids, attention_mask=context_attention_mask)
        return torch.tensor(self.pca.transform(np.array(vec.pooler_output.tolist())), dtype=torch.float32)

    def get_query_vector(self, query_input_ids, query_attention_mask):
        vec = self.query_model(input_ids=query_input_ids, attention_mask=query_attention_mask)
        return torch.tensor(self.pca.transform(np.array(vec.pooler_output.tolist())), dtype=torch.float32)



class DPRReducedPCA512(DPRReducedPCA256):
    """docstring for DPRReducedLayerNormTransformerWeights512."""

    def __init__(self, dense_size=768, freeze_params=0.0):
        super(DPRReducedPCA256, self).__init__(dense_size, freeze_params)
        with open(os.path.dirname(__file__) + "/pca_512.p", 'rb') as target:
            self. pca = pickle.load(target)
        self.dense_size = 512


class DPRReducedPCA128(DPRReducedPCA256):
    """docstring for DPRReducedLayerNormTransformerWeights512."""

    def __init__(self, dense_size=768, freeze_params=0.0):
        super(DPRReducedPCA128, self).__init__(dense_size, freeze_params)
        with open(os.path.dirname(__file__) + "/pca_128.p", 'rb') as target:
            self. pca = pickle.load(target)
        self.dense_size = 128



class DPRScratch(DotProductModel):
    def __init__(self, dense_size=768, freeze_params=0.0):
        '''
        :dense_size : the dimension to which the DPR has to encode
        :freeze_params : the percentage of the parameters to be frozen
        '''
        super(DPRScratch, self).__init__()
        self.dense_size = dense_size
        self.ctx_tokenizer = BertTokenizerFast.from_pretrained('bert-base-uncased')
        self.passage_model = BertModel.from_pretrained("bert-base-uncased")
        self.question_tokenizer = self.ctx_tokenizer
        self.query_model = BertModel.from_pretrained("bert-base-uncased")

        self.freeze_params = freeze_params

        self.log_softmax = nn.LogSoftmax(dim=1)

    def get_context_vectors(self, context_input_ids, context_attention_mask):
        vec = self.passage_model(input_ids=context_input_ids, attention_mask=context_attention_mask)
        return vec.pooler_output

    def get_query_vector(self, query_input_ids, query_attention_mask):
        vec = self.query_model(input_ids=query_input_ids, attention_mask=query_attention_mask)
        return vec.pooler_output

    def forward(self, context_input_ids, context_attention_mask, query_input_ids, query_attention_mask):
        dense_query = self.query_model(input_ids=query_input_ids, attention_mask=query_attention_mask)
        dense_passage = self.passage_model(input_ids=context_input_ids.squeeze(), attention_mask=context_attention_mask.squeeze())

        dense_query = dense_query['pooler_output']
        dense_passage = dense_passage['pooler_output']

        similarity_matrix = torch.matmul(dense_query, torch.transpose(dense_passage, 0, 1))

        batchsize = similarity_matrix.size()[0]
        labels = torch.tensor(list(range(batchsize)))

        logits = self.log_softmax(similarity_matrix)
        return logits, labels.flatten()
