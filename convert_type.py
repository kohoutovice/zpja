# Author: Jan Jurca xjurca08@studs.fit.vutbr.cz


import argparse
import pickle
import glob
import numpy as np
import pathlib
from sklearn.decomposition import PCA

parser = argparse.ArgumentParser()
parser.add_argument("--glob", action="store", help="Glob expresion for imput file")
args = parser.parse_args()

for filename in glob.glob(args.glob):
    data = None
    with open(filename, 'rb') as f:
        print("Loading:", filename)
        data = pickle.load(f)
        data = [(x[0],np.array(x[1], dtype=np.float32)) for x in data]

    if data:
        with open(filename, 'wb') as target:
            pickle.dump(data, target)
