# Author: Jan Jurca xjurca08@studs.fit.vutbr.cz


from datasets import load_dataset
from torch.utils.data import Dataset
import torch
import os
import matplotlib.pyplot as plt
import numpy as np
import json

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

def get_data(dev=False, all=False, tdev=False):
    datasets = load_dataset("squad")
    train_questions = []
    train_contexts = []

    dev_questions = []
    dev_contexts = []

    val_questions = []
    val_contexts = []


    for i, entry in enumerate(datasets['train']):
        train_contexts.append(entry['context'])
        train_questions.append(entry['question'])
    for i, entry in enumerate(datasets['validation']):
        val_contexts.append(entry['context'])
        val_questions.append(entry['question'])

    dev_split = 87000 if dev else 80000
    train_split = 400 if dev else 80000
    dev_questions = train_questions[dev_split:]
    dev_contexts = train_contexts[dev_split:]

    if not all:
        train_questions = train_questions[:train_split]
        train_contexts = train_contexts[:train_split]

    if dev:
        print("Validation len", len(val_contexts))
        val_contexts = val_contexts[:500]
        val_questions = val_questions[:500]

    if tdev:
        dev_questions = val_questions
        dev_contexts = val_contexts

    return train_questions, train_contexts, dev_questions, dev_contexts, val_questions, val_contexts



class DatasetCustom(Dataset):
    def __init__(self,ctxs, questions):
        super(DatasetCustom, self).__init__()
        self.ctxs = ctxs
        self.questions = questions

    def __getitem__(self,index):
        context_input_ids = self.ctxs.input_ids[index]
        context_attention_mask = self.ctxs.attention_mask[index]
        question_input_ids = self.questions.input_ids[index]
        question_attention_mask = self.questions.attention_mask[index]

        return question_input_ids.to(device), question_attention_mask.to(device), context_input_ids.to(device), context_attention_mask.to(device)

    def __len__(self):
        return len(self.ctxs.input_ids)


class DatasetCTX(Dataset):
    def __init__(self,ctxs):
        super(DatasetCTX, self).__init__()
        self.ctxs = ctxs

    def __getitem__(self,index):
        context_input_ids = self.ctxs.input_ids[index]
        context_attention_mask = self.ctxs.attention_mask[index]

        return context_input_ids.to(device), context_attention_mask.to(device)

    def __len__(self):
        return len(self.ctxs.input_ids)



def load_checkpoint(model, optimizer,filename):
    # Note: Input model & optimizer should be pre-defined.  This routine only updates their states.
    start_epoch = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename)
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (epoch {})".format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer, start_epoch


def plot(data, name='plot.png', show=False):
    '''
    data = [
        {'name': name, topk:[(k, topk(k)), ...]},
        ...
    ]
    '''
    for line in data:
        plt.plot([x[0] for x in line['topk']], [x[1] for x in line['topk']], label=line['name'])
    plt.title('')
    plt.xlabel('k')
    plt.ylabel('Top-k accuracy [%]')
    plt.legend()
    plt.savefig(name)
    if show:
        plt.show()
